CREATE TABLE guild
(
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE user
(
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE role
(
    id INTEGER PRIMARY KEY,
    guild_id INTEGER,
    name TEXT NOT NULL,
    FOREIGN KEY(guild_id) REFERENCES guild(id) ON DELETE CASCADE
);

CREATE TABLE channel
(
    id INTEGER PRIMARY KEY,
    guild_id INTEGER,
    name TEXT NOT NULL,
    FOREIGN KEY(guild_id) REFERENCES guild(id) ON DELETE CASCADE
);


CREATE TABLE rolegroup
(
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE rolegroup_role
(
    rolegroup_id INTEGER,
    role_id INTEGER,
    PRIMARY KEY(rolegroup_id, role_id),
    FOREIGN KEY(rolegroup_id) REFERENCES rolegroup(id) ON DELETE CASCADE,
    FOREIGN KEY(role_id) REFERENCES role(id) ON DELETE CASCADE
);

CREATE TABLE guild_user
(
    guild_id INTEGER,
    user_id INTEGER,
    PRIMARY KEY(guild_id, user_id),
    FOREIGN KEY(guild_id) REFERENCES guild(id) ON DELETE CASCADE,
    FOREIGN KEY(user_id) REFERENCES user(id) ON DELETE CASCADE
);

CREATE TABLE user_role
(
    user_id INTEGER,
    role_id INTEGER,
    PRIMARY KEY(user_id, role_id),
    FOREIGN KEY(user_id) REFERENCES user(id) ON DELETE CASCADE,
    FOREIGN KEY(role_id) REFERENCES role(id) ON DELETE CASCADE
);
