import discord
from discord.ext import commands
import logging
import persist
from misc import SuccessEmbed, FailureEmbed


class RejoinRoleError(Exception): pass
class LeaveUnjoinedRoleError(Exception): pass


class MainCog(commands.Cog):

    def __init__(self, bot, conn):
        self.bot = bot
        self.conn = conn

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        logging.warning(f"Joining server {guild.id} with name {guild.name}")

        persist.insert_guilds(self.conn, guild)
        persist.insert_users(self.conn, *guild.members)
        persist.insert_channels(self.conn, *guild.channels)
        persist.insert_roles(self.conn, *guild.roles)

        guild_users = [(guild, member) for member in guild.members]
        persist.insert_guild_users(self.conn, *guild_users)

        user_roles = [(member, role) for member in guild.members for role in member.roles]
        persist.insert_user_roles(self.conn, *user_roles)

    @commands.Cog.listener()
    async def on_guild_remove(self, guild):
        logging.warning(f"Leaving server {guild.id} with name {guild.name}")

        persist.delete_guilds(self.conn, guild.id)

        users_to_delete = set(guild.members) - set(self.bot.users)
        user_ids_to_delete = [user.id for user in users_to_delete]
        persist.delete_users(self.conn, *user_ids_to_delete)

    @commands.Cog.listener()
    async def on_guild_update(self, before, after):
        logging.warning(f"The server {before.id} has been updated")
        persist.upsert_guilds(self.conn, after)

    @commands.Cog.listener()
    async def on_member_join(self, member):
        logging.warning(f"The user {member.id} has joined the server")

        persist.insert_users(self.conn, member)
        persist.insert_guild_users(self.conn, (member.guild, member))

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        logging.warning(f"The user {member.id} has left")

        persist.delete_guild_users(self.conn, (member.guild.id, member.id))
        if member not in self.bot.users:
            persist.delete_users(self.conn, member.id)

    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        if (before.name != after.name):
            logging.warning(f"The user {before.id} has been updated")
            logging.warning(f"Before: {before.name}, After: {after.name}")

        persist.upsert_users(self.conn, after)

        new_roles = set(after.roles) - set(before.roles)
        user_roles_to_add = [(after, role) for role in new_roles]
        persist.insert_user_roles(self.conn, *user_roles_to_add)

        old_roles = set(before.roles) - set(after.roles)
        user_roles_to_del = [(after.id, role.id) for role in old_roles]
        persist.delete_user_roles(self.conn, *user_roles_to_del)

    @commands.Cog.listener()
    async def on_guild_channel_create(self, channel):
        logging.warning(f"The channel {channel.name} has been created")
        persist.insert_channels(self.conn, channel)

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel):
        logging.warning(f"The channel {channel.name} has been deleted")
        persist.delete_channels(self.conn, channel.id)

    @commands.Cog.listener()
    async def on_guild_channel_update(self, before, after):
        logging.warning(f"The channel {before.name} has been updated")
        persist.upsert_channels(self.conn, after)

    @commands.Cog.listener()
    async def on_guild_role_create(self, role):
        logging.warning(f"The role {role.name} has been created")
        persist.insert_roles(self.conn, role)

    @commands.Cog.listener()
    async def on_guild_role_delete(self,  role):
        logging.warning(f"The role {role.name} has been deleted")
        persist.delete_roles(self.conn, role.id)

    @commands.Cog.listener()
    async def on_guild_role_update(self, before, after):
        logging.warning(f"The role {before.name} has been updated")
        persist.upsert_roles(self.conn, after)

    @commands.command()
    @commands.bot_has_permissions(send_messages=True)
    async def join(self, ctx, role: discord.Role):
        member = ctx.message.author
        channel = ctx.message.channel

        if role in member.roles:
            raise RejoinRoleError

        member_roles_after_join = [role] + member.roles
        rolegroups = persist.get_rolegroups_containing_at_least(
                self.conn, 2, *member_roles_after_join)
        roles_to_remove = set()
        for rolegroup in rolegroups:
            if list(roles_to_remove) == member.roles: break
            rolegroup_roles = persist.get_rolegroup_roles(self.conn, rolegroup[0])
            rolegroup_role_ids = {r[0] for r in rolegroup_roles}
            conflicting_roles = {r for r in member.roles if r.id in rolegroup_role_ids}
            roles_to_remove.update(conflicting_roles)
        await member.remove_roles(*roles_to_remove)
        data = [(member.id, role.id) for role in roles_to_remove]
        persist.delete_user_roles(self.conn, *data)

        await member.add_roles(role)
        persist.insert_user_roles(self.conn, (member, role))

        description = "You joined the role **{}**".format(role.name)
        logging.info(f"User {member.id} joined the role {role.name}")

        embed = SuccessEmbed(description=description)
        await channel.send(embed=embed)

    @join.error
    async def join_error(self, ctx, error):
        channel = ctx.message.channel

        if isinstance(error, commands.errors.MissingRequiredArgument):
            description = "Invalid command usage"
        elif isinstance(error, commands.errors.BadArgument):
            description = "That is not a valid role"
        elif (isinstance(error, commands.errors.CommandInvokeError) and
              isinstance(error.original, RejoinRoleError)):
            description = "You already joined this role"
        elif (isinstance(error, commands.errors.CommandInvokeError) and
              isinstance(error.original, discord.errors.Forbidden)):
            description = "You are not allowed to join this role"
        else:
            description = "An unexpected error has ocurred"

        embed = FailureEmbed(description=description)
        await channel.send(embed=embed)

    @commands.bot_has_permissions(send_messages=True)
    @commands.command()
    async def leave(self, ctx, role: discord.Role):
        member = ctx.message.author
        channel = ctx.message.channel

        if role not in member.roles:
            raise LeaveUnjoinedRoleError

        await member.remove_roles(role)

        persist.delete_user_roles(self.conn, (member.id, role.id))

        description = "You left the role **{}**".format(role.name)
        logging.info(f"User {member.id} left the role {role.name}")
        embed = SuccessEmbed(description=description)
        await channel.send(embed=embed)

    @leave.error
    async def leave_error(self, ctx, error):
        channel = ctx.message.channel

        if isinstance(error, commands.errors.MissingRequiredArgument):
            description = "Invalid command usage"
        elif isinstance(error, commands.errors.BadArgument):
            description = "That is not a valid role"
        elif (isinstance(error, commands.errors.CommandInvokeError) and
              isinstance(error.original, LeaveUnjoinedRoleError)):
            description = "You have not joined this role"
        elif (isinstance(error, commands.errors.CommandInvokeError) and
              isinstance(error.original, discord.errors.Forbidden)):
            description = "You are not allowed to leave this role"
        else:
            description = "An unexpected error has ocurred"

        embed = FailureEmbed(description=description)
        await channel.send(embed=embed)

