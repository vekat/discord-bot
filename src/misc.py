import discord


class SuccessEmbed(discord.Embed):

    def __init__(self, description):
        green = discord.Colour.from_rgb(0, 255, 0)
        super().__init__(description=description, colour=green)


class FailureEmbed(discord.Embed):

    def __init__(self, description):
        red = discord.Colour.from_rgb(255, 0, 0)
        super().__init__(description=description, colour=red)
