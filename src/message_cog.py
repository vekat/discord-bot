import persist
from discord.ext import commands


class MessageCog(commands.Cog):

    def __init__(self, bot, conn):
        self.bot = bot
        self.conn = conn

    @commands.Cog.listener()
    async def on_message(self, message):
        persist.insert_messages(self.conn, message)

    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
        persist.upsert_messages(self.conn, after)

