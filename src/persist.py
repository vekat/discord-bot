import datetime


def query_tables(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    return cursor.fetchall()


def insert_messages(conn, *messages):
    if not messages: return

    epoch = datetime.datetime.utcfromtimestamp(0)

    def unix_time_in_sec(dt):
        return (dt - epoch).total_seconds()

    def get_message_data(message):
        channel = message.channel
        user = message.author
        created_at_timestamp = int(unix_time_in_sec(message.created_at))
        edited_at = message.edited_at
        edited_at_timestamp = None if edited_at is None else int(unix_time_in_sec(edited_at))
        return (
            message.id,
            channel.id,
            user.id,
            message.content,
            created_at_timestamp,
            edited_at_timestamp
        )

    data = map(get_message_data, messages)
    sql = (
        "INSERT OR IGNORE INTO message("
        "id, channel_id, user_id, content, created_at, edited_at) "
        "values (?,?,?,?,?,?)"
    )
    conn.executemany(sql, data)
    conn.commit()


def update_messages(conn, *messages):
    if not messages: return

    epoch = datetime.datetime.utcfromtimestamp(0)

    def unix_time_in_sec(dt):
        return (dt - epoch).total_seconds()

    def get_message_data(message):
        edited_at = message.edited_at
        edited_at_timestamp = None if edited_at is None else int(unix_time_in_sec(edited_at))
        return (message.content, edited_at_timestamp, message.id)

    data = map(get_message_data, messages)
    sql = "UPDATE message SET content = ?, edited_at = ? WHERE id = ?"
    conn.executemany(sql, data)
    conn.commit()


def upsert_messages(conn, *messages):
    insert_messages(conn, *messages)
    update_messages(conn, *messages)


def delete_messages(conn, *ids):
    if not ids: return

    sql = "DELETE FROM message WHERE id = ?"
    conn.executemany(sql, ((id,) for id in ids))
    conn.commit()


def query_guilds(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM guild")
    return cursor.fetchall()


def insert_guilds(conn, *guilds):
    if not guilds: return

    data = tuple((guild.id, guild.name) for guild in guilds)
    sql = "INSERT OR IGNORE INTO guild(id, name) values (?,?)"
    conn.executemany(sql, data)
    conn.commit()


def update_guilds(conn, *guilds):
    if not guilds: return

    data = tuple((guild.name, guild.id) for guild in guilds)
    sql = "UPDATE guild SET name = ? WHERE id = ?"
    conn.executemany(sql, data)
    conn.commit()


def upsert_guilds(conn, *guilds):
    insert_guilds(conn, *guilds)
    update_guilds(conn, *guilds)


def delete_guilds(conn, *ids):
    if not ids: return

    sql = "DELETE FROM guild WHERE id = ?"
    conn.executemany(sql, ((id,) for id in ids))
    conn.commit()


def sync_guilds(conn, guilds):
    if not guilds: return

    guild_ids_stored = {row[0] for row in query_guilds(conn)}
    guild_ids_discord = {guild.id for guild in guilds}
    guild_ids_to_delete = guild_ids_stored - guild_ids_discord
    delete_guilds(conn, *guild_ids_to_delete)
    upsert_guilds(conn, *guilds)


def query_users(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM user")
    return cursor.fetchall()


def insert_users(conn, *users):
    if not users: return

    data = tuple((user.id, user.name) for user in users)
    sql = "INSERT OR IGNORE INTO user(id, name) values (?,?)"
    conn.executemany(sql, data)
    conn.commit()


def update_users(conn, *users):
    if not users: return

    data = tuple((user.name, user.id) for user in users)
    sql = "UPDATE user SET name = ? WHERE id = ?"
    conn.executemany(sql, data)
    conn.commit()


def upsert_users(conn, *users):
    insert_users(conn, *users)
    update_users(conn, *users)


def delete_users(conn, *ids):
    if not ids: return

    sql = "DELETE FROM user WHERE id = ?"
    conn.executemany(sql, ((id,) for id in ids))
    conn.commit()


def sync_users(conn, users):
    if not users: return

    user_ids_stored = {row[0] for row in query_users(conn)}
    user_ids_discord = {user.id for user in users}
    user_ids_to_delete = user_ids_stored - user_ids_discord
    delete_users(conn, *user_ids_to_delete)
    upsert_users(conn, *users)


def query_channels(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM channel")
    return cursor.fetchall()


def insert_channels(conn, *channels):
    data = tuple((channel.id, channel.guild.id, channel.name) for channel in channels)
    sql = "INSERT OR IGNORE INTO channel(id, guild_id, name) values(?,?,?)"
    conn.executemany(sql, data)
    conn.commit()


def update_channels(conn, *channels):
    if not channels: return

    data = tuple((channel.name, channel.id) for channel in channels)
    sql = "UPDATE channel SET name = ? WHERE id = ?"
    conn.executemany(sql, data)
    conn.commit()


def upsert_channels(conn, *channels):
    insert_channels(conn, *channels)
    update_channels(conn, *channels)


def delete_channels(conn, *ids):
    if not ids: return

    sql = "DELETE FROM channel WHERE id = ?"
    conn.executemany(sql, ((id,) for id in ids))
    conn.commit()


def sync_channels(conn, channels):
    if not channels: return

    channel_ids_stored = {row[0] for row in query_channels(conn)}
    channel_ids_discord = {channel.id for channel in channels}
    channel_ids_to_delete = channel_ids_stored - channel_ids_discord
    delete_channels(conn, *channel_ids_to_delete)
    upsert_channels(conn, *channels)


def query_roles(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM role")
    return cursor.fetchall()


def insert_roles(conn, *roles):
    if not roles: return

    data = tuple((role.id, role.guild.id, role.name) for role in roles)
    sql = "INSERT OR IGNORE INTO role(id, guild_id, name) values (?,?,?)"
    conn.executemany(sql, data)
    conn.commit()


def update_roles(conn, *roles):
    if not roles: return

    data = tuple((role.name, role.id) for role in roles)
    sql = "UPDATE role SET name = ? WHERE id = ?"
    conn.executemany(sql, data)
    conn.commit()


def upsert_roles(conn, *roles):
    insert_roles(conn, *roles)
    update_roles(conn, *roles)


def delete_roles(conn, *ids):
    if not ids: return

    sql = "DELETE FROM role WHERE id = ?"
    conn.executemany(sql, ((id,) for id in ids))
    conn.commit()


def sync_roles(conn, roles):
    if not roles: return

    role_ids_stored = {row[0] for row in query_roles(conn)}
    role_ids_discord = {role.id for role in roles}
    role_ids_to_delete = role_ids_stored - role_ids_discord
    delete_roles(conn, *role_ids_to_delete)
    upsert_roles(conn, *roles)


def get_rolegroup_from_name(conn, name):
    cursor = conn.cursor()
    sql = "SELECT * FROM rolegroup WHERE name = ?"
    cursor.execute(sql, (name,))
    return cursor.fetchone()


def get_rolegroup_from_roles(conn, *roles):
    param_list = ",".join('?' * len(roles))
    sql = (
        "SELECT rolegroup.* FROM rolegroup "
        "JOIN rolegroup_role ON rolegroup_role.rolegroup_id = rolegroup.id "
        "WHERE rolegroup_id IN ("
        "    SELECT rolegroup_id FROM rolegroup_role "
        "    WHERE role_id IN ({param_list}) "
        "    GROUP BY rolegroup_id "
        "    HAVING COUNT(*) >= {num_of_roles} ) "
        "GROUP BY id "
        "HAVING COUNT(*) = {num_of_roles}"
    ).format(param_list=param_list, num_of_roles=len(roles))
    cursor = conn.cursor()
    cursor.execute(sql, tuple(role.id for role in roles))
    return cursor.fetchone()


def get_rolegroups_containing_at_least(conn, n, *roles):
    param_list = ",".join('?' * len(roles))
    sql = (
        "SELECT rolegroup.* FROM rolegroup "
        "JOIN rolegroup_role ON rolegroup_role.rolegroup_id = rolegroup.id "
        "WHERE role_id IN ({}) "
        "GROUP BY rolegroup.id "
        "HAVING COUNT(*) >= {}"
    ).format(param_list, n)
    cursor = conn.cursor()
    cursor.execute(sql, tuple(role.id for role in roles))
    return cursor.fetchall()


def insert_rolegroup(conn, name):
    cursor = conn.cursor()
    sql = "INSERT OR IGNORE INTO rolegroup(name) values (?)"
    cursor.execute(sql, (name,))
    return cursor.lastrowid


def delete_rolegroup(conn, rolegroup_id):
    cursor = conn.cursor()
    sql = "DELETE FROM rolegroup WHERE id = ?"
    cursor.execute(sql, (rolegroup_id,))


def query_rolegroups(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM rolegroup")
    return cursor.fetchall()

def get_rolegroup_roles(conn, rolegroup_id):
    cursor = conn.cursor()
    sql = (
        "SELECT role.* FROM role "
        "JOIN rolegroup_role ON rolegroup_role.role_id = role.id "
        "WHERE rolegroup_id = ?"
    )
    cursor.execute(sql, (rolegroup_id,))
    return cursor.fetchall()


def insert_rolegroup_roles(conn, rolegroup_id, *roles):
    data = tuple((rolegroup_id, role.id) for role in roles)
    sql = (
        "INSERT OR IGNORE INTO "
        "rolegroup_role(rolegroup_id, role_id) "
        "values (?,?)"
    )
    conn.executemany(sql, data)
    conn.commit()


def delete_rolegroup_roles(conn, rolegroup_id, *roles):
    data = tuple((rolegroup_id, role.id) for role in roles)
    sql = (
        "DELETE FROM rolegroup_role "
        "WHERE rolegroup_id = ? AND role_id = ?"
    )
    conn.executemany(sql, data)
    conn.commit()


def query_guild_users(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM guild_user")
    return cursor.fetchall()


def insert_guild_users(conn, *guild_users):
    if not guild_users: return

    data = tuple((guild.id, user.id) for (guild, user) in guild_users)
    sql = "INSERT OR IGNORE INTO guild_user(guild_id, user_id) values (?,?)"
    conn.executemany(sql, data)
    conn.commit()


def delete_guild_users(conn, *data):
    if not data: return

    sql = "DELETE FROM guild_user WHERE guild_id = ? AND user_id = ?"
    conn.executemany(sql, data)
    conn.commit()


def sync_guild_users(conn, guild_users):
    if not guild_users: return

    guild_user_ids_stored = {(row[0], row[1]) for row in query_guild_users(conn)}
    guild_user_ids_discord = {(guild.id, user.id) for (guild, user) in guild_users}
    guild_user_ids_to_delete = guild_user_ids_stored - guild_user_ids_discord
    delete_guild_users(conn, *guild_user_ids_to_delete)
    insert_guild_users(conn, *guild_users)


def query_user_roles(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM user_role")
    return cursor.fetchall()


def insert_user_roles(conn, *user_roles):
    if not user_roles: return

    data = tuple((user.id, role.id) for (user, role) in user_roles)
    sql = "INSERT OR IGNORE INTO user_role(user_id, role_id) values (?,?)"
    conn.executemany(sql, data)
    conn.commit()


def delete_user_roles(conn, *data):
    if not data: return

    sql = "DELETE FROM user_role WHERE user_id = ? AND role_id = ?"
    conn.executemany(sql, data)
    conn.commit()


def sync_user_roles(conn, user_roles):
    if not user_roles: return

    user_role_ids_stored = {(row[0], row[1]) for row in query_user_roles(conn)}
    user_role_ids_discord = {(user.id, role.id) for (user, role) in user_roles}
    user_role_ids_to_delete = user_role_ids_stored - user_role_ids_discord
    delete_user_roles(conn, *user_role_ids_to_delete)
    insert_user_roles(conn, *user_roles)
