import sqlite3
import logging
import discord
from discord.ext import commands
import persist
import secret
from main_cog import MainCog
from rolegroup_cog import RolegroupCog
from message_cog import MessageCog

handlers = [logging.FileHandler('bot.log', 'a', 'utf-8')]
level = logging.INFO
fmt = '(%(name)s) %(levelname)s: %(message)s'
logging.basicConfig(handlers=handlers, level=level, format=fmt)

bot = commands.Bot(command_prefix='::', status=discord.Status.dnd)
main_conn = sqlite3.connect("main.sqlite3")
message_conn = sqlite3.connect("message.sqlite3")
main_conn.execute("PRAGMA foreign_keys = 1")


def sync_main_db(conn):
    persist.sync_guilds(conn, bot.guilds)
    persist.sync_users(conn, list(bot.get_all_members()))
    persist.sync_channels(conn, list(bot.get_all_channels()))

    roles = [role for guild in bot.guilds for role in guild.roles]
    persist.sync_roles(conn, roles)

    guild_users = []
    for guild in bot.guilds:
        for member in guild.members:
            guild_users.append((guild, member))
    persist.sync_guild_users(conn, guild_users)

    user_roles = []
    for guild in bot.guilds:
        for member in guild.members:
            for role in member.roles:
                user_roles.append((member, role))
    persist.sync_user_roles(conn, user_roles)


@bot.event
async def on_ready():
    if not persist.query_tables(main_conn):
        cursor = main_conn.cursor()
        with open('schema.sql') as f:
            cursor.executescript(f.read())

    if not persist.query_tables(message_conn):
        cursor = message_conn.cursor()
        cursor.execute("""
            CREATE TABLE message
            (
                id INTEGER PRIMARY KEY,
                channel_id INTEGER NOT NULL,
                user_id INTEGER NOT NULL,
                content TEXT NOT NULL,
                created_at INTEGER NOT NULL,
                edited_at INTEGER
            );""")

    logging.info("Starting sync")
    sync_main_db(main_conn)
    logging.info("Sync ended")

    await bot.change_presence(status=discord.Status.online)
    bot.add_cog(MainCog(bot, main_conn))
    bot.add_cog(RolegroupCog(bot, main_conn))
    bot.add_cog(MessageCog(bot, message_conn))


if __name__ == '__main__':
    bot.run(secret.TOKEN)
