import discord
from discord.ext import commands
import persist
from misc import SuccessEmbed, FailureEmbed


class RolegroupCog(commands.Cog):

    def __init__(self, bot, conn):
        self.bot = bot
        self.conn = conn

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def rolegroup_create(self, ctx, group_name: str, roles: commands.Greedy[discord.Role]):
        channel = ctx.message.channel

        if len(roles) < 2:
            description = "A rolegroup must have at least two roles"
            embed = FailureEmbed(description=description)
            await channel.send(embed=embed)
            return

        if persist.get_rolegroup_from_roles(self.conn, *roles) is not None:
            description = "There already exists a rolegroup with the roles given"
            embed = FailureEmbed(description=description)
            await channel.send(embed=embed)
            return

        rolegroup_id = persist.insert_rolegroup(self.conn, group_name)
        persist.insert_rolegroup_roles(self.conn, rolegroup_id, *roles)

        description = "Rolegroup **{}** was created".format(group_name)
        embed = SuccessEmbed(description=description)
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def rolegroup_delete(self, ctx, group_name):
        channel = ctx.message.channel
        rolegroup = persist.get_rolegroup_from_name(self.conn, group_name)
        if rolegroup is None:
            description =  "No rolegroup named **{}** exists".format(group_name)
            embed = FailureEmbed(description=description)
            await channel.send(embed=embed)
            return

        rolegroup_id = rolegroup[0]
        persist.delete_rolegroup(self.conn, rolegroup_id)

        description = "Group **{}** was deleted".format(group_name)
        embed = SuccessEmbed(description=description)
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def rolegroup_info(self, ctx, group_name):
        channel = ctx.message.channel
        rolegroup = persist.get_rolegroup_from_name(self.conn, group_name)
        if rolegroup is None:
            description =  "No rolegroup named **{}** exists".format(group_name)
            embed = FailureEmbed(description=description)
            await channel.send(embed=embed)
            return

        rolegroup_id, rolegroup_name = rolegroup
        roles = persist.get_rolegroup_roles(self.conn, rolegroup_id)

        embed = discord.Embed(title="Rolegroup info")
        embed.add_field(name="Name", value=rolegroup_name, inline=False)
        roles_field_value = ", ".join(role[2] for role in roles)
        embed.add_field(name="Roles", value=roles_field_value, inline=False)

        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def rolegroup_list(self, ctx):
        channel = ctx.message.channel
        rolegroups = persist.query_rolegroups(self.conn)
        if not rolegroups:
            description = "No rolegroups have been created"
        else:
            description = '\n'.join(rg[1] for rg in rolegroups)
        embed = discord.Embed(description=description)
        await channel.send(embed=embed)
